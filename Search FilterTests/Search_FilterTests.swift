//
//  Search_FilterTests.swift
//  Search FilterTests
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import XCTest
import CoreData

@testable import Search_Filter
class Search_FilterTests: XCTestCase {
    var engine :NetworkEngineMock?
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        engine =  NetworkEngineMock()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        engine = nil
    }
    
    
    
    func testListItempNotRefresh() {
        let errorTemp = NSError(domain:"", code:0, userInfo:["error":"no data"])
        engine?.error = errorTemp as Error
        let fetching =  Fetching(engine:engine!)
        let searchItemModel  = SearchItemsModel.init(fetching: fetching)
        searchItemModel.listItem(refresh: false, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            let error :NSError? = error! as NSError
            XCTAssertTrue(error?.userInfo["error"] != nil,"no Data")
        }) { (success) in
            
        }
        
       
    }
    
    func testListItemRefresh() {
        let errorTemp = NSError(domain:"", code:0, userInfo:["error":"no data"])
        engine?.error = errorTemp as Error
        let fetching =  Fetching(engine:engine!)
        let searchItemModel  = SearchItemsModel.init(fetching: fetching)
        searchItemModel.listItem(refresh: true, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            let error :NSError? = error! as NSError
            XCTAssertTrue(error?.userInfo["error"] != nil,"no Data")
        }) { (success) in
            
        }
        
        
    }
    
    func testNetworkList() {
        // create test with Mocking Response HTTP for Get Contacts
        
        var dataSucces:Data {
            var arrayDictionary:[[String:AnyObject]] = [[String:AnyObject]]()
            
            let dictItem1:[String:AnyObject]  = ["id":232947893 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            let dictItem2:[String:AnyObject] = ["id":232947894 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            let dictItem3:[String:AnyObject] = ["id":232947895 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            arrayDictionary.append(dictItem1)
            arrayDictionary.append(dictItem2)
            arrayDictionary.append(dictItem3)
            let dictionary:[String:AnyObject] = ["data": arrayDictionary as AnyObject]
            
            let data = (try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted))
            return data
        }
        engine?.dataResponse =  dataSucces
        let fetching =  Fetching(engine:engine!)
        let searchModel  = SearchItemsModel.init(fetching: fetching)
        searchModel.isGoldMember = false
        searchModel.listItem(refresh: true, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            
            XCTFail("It's should be success")
        }) { (success) in
            
            XCTAssertTrue(success, "It should be success with 3 dictionary")
        }
        
    }
    
    
    func testNetworkListDataListZero() {
        // create test with Mocking Response HTTP for Get Contacts
        
        var dataSucces:Data {
            let arrayDictionary:[[String:AnyObject]] = [[String:AnyObject]]()
            
            
            let dictionary:[String:AnyObject] = ["data": arrayDictionary as AnyObject]
            
            let data = (try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted))
            return data
        }
        engine?.dataResponse =  dataSucces
        let fetching =  Fetching(engine:engine!)
        let searchModel  = SearchItemsModel.init(fetching: fetching)
        searchModel.isGoldMember = false
        searchModel.listItem(refresh: true, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            
            XCTAssertTrue(error != nil,"It's  success error")
        }) { (success) in
            
            XCTFail("It should be error ")
        }
        
    }
    
    func testNetworkListNoValueData() {
        // create test with Mocking Response HTTP for Get Contacts
        
        var dataSucces:Data {
            
            
            
            let dictionary:[String:AnyObject] = ["status": "" as AnyObject]
            
            let data = (try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted))
            return data
        }
        engine?.dataResponse =  dataSucces
        let fetching =  Fetching(engine:engine!)
        let searchModel  = SearchItemsModel.init(fetching: fetching)
        searchModel.isGoldMember = false
        searchModel.listItem(refresh: true, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            
            XCTAssertTrue(error != nil,"It's  success error")
        }) { (success) in
            
            XCTFail("It should be error ")
        }
        
    }
    
    func testNetworkList2() {
        // create test with Mocking Response HTTP for Get Contacts
        
        var dataSucces:Data {
            var arrayDictionary:[[String:AnyObject]] = [[String:AnyObject]]()
            
            let dictItem1:[String:AnyObject]  = ["id":232947893 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            let dictItem2:[String:AnyObject] = ["id":232947894 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            let dictItem3:[String:AnyObject] = ["id":232947895 ,"name":"TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241","uri":"https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1","image_uri":"https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","image_uri_700":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png","price":"Rp 88.400"] as [String:AnyObject]
            arrayDictionary.append(dictItem1)
            arrayDictionary.append(dictItem2)
            arrayDictionary.append(dictItem3)
            let dictionary:[String:AnyObject] = ["data": arrayDictionary as AnyObject]
            
            let data = (try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted))
            return data
        }
        engine?.dataResponse =  dataSucces
        let fetching =  Fetching(engine:engine!)
        let searchModel  = SearchItemsModel.init(fetching: fetching)
        searchModel.listItem(refresh: true, refreshView: {
            
        }, beginUpdateView: {
            
        }, failed: { (error) in
            
            XCTFail("It's should be success")
        }) { (success) in
            
            searchModel.rows = 0
            searchModel.isGoldMember = true
            searchModel.listItem(refresh: true, refreshView: {
                
            }, beginUpdateView: {
                
            }, failed: { (error) in
                
                XCTFail("It's should be success")
            }) { (success) in
                
                XCTAssertTrue(success, "It should be success ")
            }
        }
    }
    
    
    /*
     data: [
     {
     id: 232947893,
     name: "TOP! Samsung J5 Pro/ J5 2017 Baby Skin Ultra Thin Hard Case 1241",
     uri: "https://www.tokopedia.com/theonecasing/top-samsung-j5-pro-j5-2017-baby-skin-ultra-thin-hard-case-1241?trkid=f%3DCa0000L000P1W0S0Sh00Co0Po0Fr0Cb0_page%3D1_ob%3D32_q%3Dsamsung_catid%3D69_po%3D1",
     image_uri: "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png",
     image_uri_700: "https://ecs7.tokopedia.net/img/cache/700/product-1/2017/11/21/22051114/22051114_ae8c1c71-f661-40c1-8136-2e854333efce_790_935.png",
     price: "Rp 88.400",
     }
     ]
    */
    class NetworkEngineMock: NetworkEngine {
        var error:Error?
        var dataResponse:Data?
        var Urlresponse:HTTPURLResponse?
        typealias Handler = NetworkEngine.Handler
        
        var requestedURL: URL?
        
        func performRequest(for url: URL, method: String?, data: Data?, completionHandler: @escaping NetworkEngine.Handler) {
            requestedURL = url
            Urlresponse = Urlresponse == nil ? HTTPURLResponse(url: url, statusCode: 200, httpVersion:nil , headerFields: nil): Urlresponse
            
            completionHandler(dataResponse,Urlresponse,error)
        }
    }
    
    
}
