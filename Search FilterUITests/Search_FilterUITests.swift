//
//  Search_FilterUITests.swift
//  Search FilterUITests
//
//  Created by David Trivian S on 11/30/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import XCTest

class Search_FilterUITests: XCTestCase {
    let app = XCUIApplication()
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func waitForElementToAppear(_ element: XCUIElement) -> Bool {
        let predicate = NSPredicate(format: "exists == true")
        let expectationX = expectation(for: predicate, evaluatedWith: element,
                                       handler: nil)
        
        let result = XCTWaiter().wait(for: [expectationX], timeout: 5)
        return result == .completed
    }
    
    func refreshCondition(){
        let collectionViews = app.collectionViews.element(boundBy: 0)
        let firstCell = collectionViews.cells.element(boundBy: 0)
        
        let start:XCUICoordinate = firstCell.coordinate(withNormalizedOffset: (CGVector(dx:0, dy:0)))
        let finish:XCUICoordinate = firstCell.coordinate(withNormalizedOffset: (CGVector(dx:0, dy:6)))
        start.press(forDuration: 0, thenDragTo: finish)
        if waitForElementToAppear(firstCell) {
            //handle timeout
            
            
        }
    }
    
    
    func testRefresh()  {
        
        refreshCondition()
      
    }
    
    func testFilter(){
        
        app.buttons["Filter"].tap()
        XCUIDevice.shared.orientation = .portrait
        
        let elementsQuery = app.scrollViews.otherElements
        let switch2 = elementsQuery.switches["0"]
        switch2.tap()
        
        let rpRpRpRpElement = elementsQuery.otherElements["Rp. , Rp. , Rp. , Rp. "]
        rpRpRpRpElement.tap()
        rpRpRpRpElement.tap()
        elementsQuery.staticTexts["Shop Type"].tap()
        
        let goldMerchantStaticText = elementsQuery.staticTexts["Gold Merchant"]
        goldMerchantStaticText.tap()
        
        let officialStoreStaticText = elementsQuery.staticTexts["Official Store"]
        officialStoreStaticText.tap()
        goldMerchantStaticText.tap()
        officialStoreStaticText.tap()
        
        let applyButton = app.buttons["Apply"]
        applyButton.tap()
       
        
    }
    
    func testReset() {
    
        let filterButton = app.buttons["Filter"]
        filterButton.tap()
        
        let scrollViewsQuery = app.scrollViews
        let elementsQuery = scrollViewsQuery.otherElements
        
        let switch2 = elementsQuery.switches["0"]
        
        switch2.tap()
        
        let resetButton = app.buttons["Reset"]
        resetButton.tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.buttons["X"].tap()
        filterButton.tap()
        elementsQuery.staticTexts["Shop Type"].tap()
        elementsQuery.staticTexts["Official Store"].tap()
        resetButton.tap()
        app.buttons["X"].tap()
        
        
    }
    
}
