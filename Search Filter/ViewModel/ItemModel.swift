//
//  ItemModel.swift
//  Search Filter
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import Foundation
import CoreData
class ItemModel {
    
    init(parseDictorinary dictionary:[String:AnyObject]?, context aContext:NSManagedObjectContext,saveToCoreData:Bool,item:Item? = nil) {
        
        if saveToCoreData == true {
            context = aContext
        }
        if dictionary != nil {
            isSaveCoreData =  saveToCoreData
            self.idServer = dictionary!["id"] as? Int32 ?? 0
            self.imageThumbnailUrl = dictionary!["image_uri"] as? String ?? ""
            self.imageFullUrl =  dictionary!["image_uri_700"] as? String ?? ""
            self.price =  dictionary!["price"] as? String ?? ""
            self.name =  dictionary!["name"] as? String ?? ""
            self.start = 0
            if isSaveCoreData == true && item != nil {
                self.item = item
                self.item?.idServer = self.idServer
                self.item?.imageThumbnailUrl = self.imageThumbnailUrl
                self.item?.imageFullUrl = self.imageFullUrl
                self.item?.price =  self.price
                self.item?.name = self.name
                self.item?.start = self.start
            }
           
            
        }
    }
    var context:NSManagedObjectContext? = nil
    var isSaveCoreData:Bool = false 
    var start:Int16 = 0 {
        willSet {
            item?.start =  newValue
        }
    }
    var item:Item? = nil {
        willSet {
            if (newValue != nil) {
                self.isSaveCoreData =  true
                self.idServer = newValue?.idServer ?? 0
                self.imageThumbnailUrl = newValue?.imageThumbnailUrl ?? ""
                self.imageFullUrl =  newValue?.imageFullUrl ?? ""
                self.name =  newValue?.name ?? ""
                self.price =  newValue?.price ?? ""
                self.start =  (newValue?.start) ?? 0
            }
        }
    }
    var idServer:Int32 = 0 {
        
        willSet {
            item?.idServer = idServer
        }
    }
    var imageThumbnailUrl: String? = "" {
        willSet {
            item?.imageThumbnailUrl = newValue ?? ""
        }
    }
    
    var imageFullUrl: String? = "" {
        willSet {
            item?.imageFullUrl = newValue ?? ""
        }
    }
    var price: String? = "" {
        willSet {
            item?.price = newValue ?? ""
        }
    }
    var name:String? = "" {
        willSet {
            item?.name = newValue ?? ""
        }
    }
    
    
}
