//
//  SearchItemModel.swift
//  Search Filter
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class SearchItemsModel {
    
    var keyword:String = Constant.Search_Keyword
    var pmin:String = "\(Constant.Min_Price)"
    var pmax:String = "\(Constant.Max_Price)"
    var isWholeSale:Bool = false
    var isOfficial:Bool = false
    var cache:NSCache<AnyObject, AnyObject>!
    let fetcher: Fetching?
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var isGoldMember:Bool = false{
        willSet {
            if newValue == true {
                fshop = "2"
            }else{
                fshop = ""
            }
        }
    }
    var fshop:String = ""
    var page:Int = 0
    var start:Int16 = 0
    var rows:Int = 10
    
    init(fetching:Fetching = Fetching()) {
        self.fetcher =  fetching
        self.cache = NSCache()
    }
    var items:[ItemModel] = [ItemModel]()
    var refresh:Bool =  false
    var needUpdate:Bool = false
    
    var context:NSManagedObjectContext? {
        
        return appDelegate.managedObjectContext
        
    }
    //get list with  start predicate from Core Data
    func listItem() -> [ItemModel]?{
        
        let fetchRequest : NSFetchRequest<Item> = Item.fetchRequest()
        let predicate = NSPredicate(format: "start == \(start)")
        fetchRequest.predicate  = predicate
        var resultItem :[Item]?
        resultItem = (try! self.context?.fetch(fetchRequest))
        var result = [ItemModel]()
        for item in resultItem! {
            let itemModel = ItemModel.init(parseDictorinary: nil, context: self.context!, saveToCoreData: true)
            itemModel.item =  item
            result.append(itemModel)
        }
        return result
        
    }
    
    //get all list from Core Data
    func listAllItem() -> [Item]?{
        
        let fetchRequest : NSFetchRequest<Item> = Item.fetchRequest()
        var resultItem :[Item]?
        resultItem = (try! self.context?.fetch(fetchRequest))
        return resultItem
        
    }
    
    //handle error on VC
    func errorHandle(_ error:Error?) -> String{
        let convertedError = error! as NSError
        if  convertedError.domain == NSURLErrorDomain && convertedError.code ==  NSURLErrorNotConnectedToInternet{
            return "No Internet Connection"
        }else{
            var message = "404"
            let userInfo:[String:AnyObject]? = (convertedError.userInfo) as [String : AnyObject]
            if userInfo != nil {
                
                if let messageErrors:[String] = userInfo!["errors"] as? [String]{
                    message = messageErrors.first!
                }else if let messageError =  userInfo!["error"]{
                    message =  messageError as! String
                }
                
            }
            return message
        }
        
        
    }
    
    //number of row
    func numberOfRows(inSection section: Int) -> Int  {
        
        return self.items.count
    }
    
    //get Item in IndexPath
    func itemForRow(at indexPath: IndexPath) -> ItemModel {
        
        return self.items[indexPath.row]
        
    }
    //fetch list from server or coreData or refresh it
    func listItem(refresh:Bool,refreshView:@escaping () -> Void,beginUpdateView:  @escaping () -> Void,failed:  @escaping (Error?) -> Void,success: @escaping (Bool) -> Void){
        var items = [ItemModel]()
        if refresh {
            
        }else{
            items = listItem()!
        }
        
        if items.count == 0 {
            self.listItemFromServer(refresh:refresh,refreshView:refreshView,beginUpdateView: beginUpdateView, failed: failed, success: { (arrayDictionary) in
                if arrayDictionary != nil {
                    self.start +=  Int16(self.rows)
                    success(true)
                }
            })
        }else{
            self.items.append(contentsOf: items)
            self.start +=  Int16(self.rows)
            success(true)
            
        }
    }
    
    
    //fetch list from Server
    func listItemFromServer(refresh:Bool,refreshView:@escaping () -> Void,beginUpdateView:  @escaping () -> Void,failed:  @escaping (Error?) -> Void,success: @escaping ([[String:AnyObject]]?) -> Void){
        
        let stringURL:String = "\(Constant.RootServer)/\(Constant.Path_Product)?q=\(keyword)&pmin=\(pmin)&pmax=\(pmax)&wholesale=\(isWholeSale)&official=\(isOfficial)&fshop=\(fshop)&start=\(refresh ? 0 : start)&rows=\(rows)"
        beginUpdateView()
        self.fetcher?.fetch(withQueryString: stringURL, failure: { (error) in
            failed(error)
            
        }) { (dictionary) in
            
            if let arrayDictionary:[[String : AnyObject]] = dictionary["data"] as? [[String : AnyObject]] {
                
                var newItems = [ItemModel]()
                self.context?.performAndWait {
                    if refresh{
                        var allItems = self.listAllItem()
                        
                        for item in allItems!{
                            
                            self.context?.delete(item)
                            
                            (try! self.context?.save())
                        }
                        
                        allItems?.removeAll()
                        self.items.removeAll()
                        
                        self.start = 0
                        refreshView()
                        
                    }
                    
                    for aDictionary  in arrayDictionary {
                        
                        if aDictionary["id"] != nil && aDictionary["id"] is Int32 {
                            
                            let itemModel =  ItemModel.init(parseDictorinary: aDictionary, context: self.context!, saveToCoreData: true,item:Item(context:self.context!))
                            itemModel.start = self.start
                            (try! self.context?.save())
                            newItems.append(itemModel)
                            
                        }
                    }
                    
                    
                    if newItems.count > 0 {
                        
                        self.items.append(contentsOf: newItems)
                        success(arrayDictionary)
                    }else{
                        
                        let errorTemp = NSError(domain:"", code:0, userInfo:["error":"No List"])
                        
                        failed(errorTemp)
                    }
                }
            }else{
                let errorTemp = NSError(domain:"", code:0, userInfo:["error":"No List"])
                
                failed(errorTemp)
                
            }
            
        }
    }
    //save cache image
    func cacheImage(stringURl:String?, completionHandler:  @escaping (Bool,UIImage?) -> Swift.Void){
        if (self.cache.object(forKey:stringURl as AnyObject) != nil){
            
            // Use Image from cache
            completionHandler(true, self.cache.object(forKey: stringURl as AnyObject) as? UIImage)
        }else{
            
            guard let stringURl = stringURl, (stringURl.count) > 0 else {
                completionHandler(false,nil)
                return
            }
            //try download images
            let url:URL! = URL(string: stringURl)
            let urlRequest = URLRequest(url: url)
            
            URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) -> Void in
                
                if let response = response, let data = data, response.isHTTPResponseValid() {
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        let img:UIImage! = UIImage(data: data)
                        if img != nil {
                            
                            self.cache.setObject(img, forKey: stringURl as AnyObject)
                            completionHandler(true,img)
                            
                        }else{
                            
                            completionHandler(false,nil)
                            
                        }
                        
                    })
                }
                
            }).resume()
            
        }
        
    }
    
    
}
