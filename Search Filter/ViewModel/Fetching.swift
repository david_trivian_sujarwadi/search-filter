//
//  Fetching.swift
//  Contacts
//
//  Created by David Trivian S on 9/11/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import Foundation
import UIKit

protocol FetchingProtocol {
    func fetch(withQueryString queryString:String,failure: @escaping ((Error)?) -> Void, success: @escaping ([String:AnyObject]) -> Void)
     func fetchList(withQueryString queryString: String, failure: @escaping ((Error)?) -> Void, success: @escaping ([[String:AnyObject]]) -> Void)
    func fetchPost(withQueryString queryString: String, params:[String:AnyObject]?,failure: @escaping ((Error)?) -> Void, success: @escaping ([String:AnyObject]) -> Void)
    func fetchPut(withQueryString queryString: String, params:[String:AnyObject]?,failure: @escaping ((Error)?) -> Void, success: @escaping ([String:AnyObject]) -> Void)
    
    
}
class Fetching:FetchingProtocol{
    private let engine: NetworkEngine
    init(engine:NetworkEngine = URLSession.shared) {
        self.engine =  engine
    }
    
    //fetch Put from server
    func fetchPut(withQueryString queryString: String, params:[String:AnyObject]?,failure: @escaping ((Error)?) -> Void, success: @escaping ([String : AnyObject]) -> Void) {
        
        
        let encoded = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: encoded)!
          let dataParams:Data? =  params != nil ? (try? JSONSerialization.data(withJSONObject: params!)) : nil
        engine.performRequest(for: url,method: "PUT", data: dataParams) { (data, response, xerror) in
            
        
            guard response?.isHTTPResponseValid() == true else {
                let errorInfo = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject])
                
                let httpResponse = response as! HTTPURLResponse
                
                let errorTemp = NSError(domain:"", code:httpResponse.statusCode, userInfo:errorInfo)
                failure(errorTemp)
                
                return
            }
            guard data != nil else{
                
                failure(xerror)
                
                return
            }
            do {
                let object = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                
                
                success(object!)
                
            } catch  {
            
                failure(error)
            
            }
        }
    }
    //fetch Put from server
    func fetchPost(withQueryString queryString: String, params:[String:AnyObject]?,failure: @escaping ((Error)?) -> Void, success: @escaping ([String : AnyObject]) -> Void) {
        let encoded = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: encoded)!
        let dataParams:Data? =  params != nil ? (try? JSONSerialization.data(withJSONObject: params!)) : nil
        engine.performRequest(for: url,method: "POST", data: dataParams) { (data, response, xerror) in
            
            
            guard xerror == nil else {
                failure(xerror)
                return
            }
            guard response?.isHTTPResponseValid() == true else {
                
                 let errorInfo = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject])
                
                let httpResponse = response as! HTTPURLResponse
                
                let errorTemp = NSError(domain:"", code:httpResponse.statusCode, userInfo:errorInfo)
                failure(errorTemp)
                return
            }
            guard data != nil else{
                
                failure(xerror)
                return
                
            }
            do {
                let object = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                
                
                success(object!)
                
            } catch  {
                
                failure(error)
                
            }
        }
    }
    
    //fetch query json
    func fetch(withQueryString queryString: String, failure: @escaping ((Error)?) -> Void, success: @escaping ([String:AnyObject]) -> Void) {
        
        let encoded = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: encoded)!
      
        engine.performRequest(for: url, method: nil, data: nil) { (data, response, xerror) in
            guard xerror == nil else {
                failure(xerror)
                return
            }
           
            guard let newResponse =  response  , newResponse.isHTTPResponseValid() == true else {
                if xerror != nil {
                    failure(xerror)
                }else{
                let errorInfo = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject])
                
                let httpResponse = response as! HTTPURLResponse
                
                let errorTemp = NSError(domain:"", code:httpResponse.statusCode, userInfo:errorInfo)
                failure(errorTemp)
                }
                return
            }
            guard data != nil else{
                failure(xerror)
                return
            }
           
            do {
                let object = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject]
                
                
                success(object!)
                
            } catch  {
                
                failure(error)
                
            }
        }
    }
    //fetch List from server
    func fetchList(withQueryString queryString: String, failure: @escaping ((Error)?) -> Void, success: @escaping ([[String:AnyObject]]) -> Void) {
        
        let encoded = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: encoded)!
        
        engine.performRequest(for: url, method: nil, data: nil) { (data, response, xerror) in
            
            
            guard xerror == nil else {
                failure(xerror)
                return
            }
            
            guard response?.isHTTPResponseValid() == true else {
                let errorInfo = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:AnyObject])
            
                let httpResponse = response as! HTTPURLResponse
                
                let errorTemp = NSError(domain:"", code:httpResponse.statusCode, userInfo:errorInfo)
                failure(errorTemp)
                return
            }
            guard data != nil else{
                failure(xerror)
                return
            }
            do {
                let object = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [[String:AnyObject]]
                
                
                success(object!)
                
            } catch  {
                
                failure(error)
                
            }
        }
    }
}

protocol NetworkEngine {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void

    func performRequest(for url: URL, method:String?, data:Data?, completionHandler: @escaping Handler)
}
//class Handle can be Mocking
extension URLSession: NetworkEngine {
   
    typealias Handler = NetworkEngine.Handler
   
    func performRequest(for urlRequest: URL, method:String?,data:Data?,completionHandler: @escaping Handler) {
       
        var request = URLRequest(url: urlRequest)
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")

        
        if method?.uppercased() == "POST" {
            request.httpMethod = "POST"
        }else if method?.uppercased() == "PUT" {
            request.httpMethod = "PUT"
        }else{
            request.httpMethod = "GET"
        }
        
        if data != nil {
        request.httpBody =  data
        }
        
        let task =  dataTask(with: request, completionHandler: completionHandler)
        task.resume()
        
    }

}
