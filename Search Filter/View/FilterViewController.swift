//
//  FilterViewController.swift
//  Search Filter
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import UIKit
import QuartzCore
protocol FilterViewDelegate:class {
    func refreshView()
}
class FilterViewController: UIViewController,TTRangeSliderDelegate,ShopTypeDelegate {
    weak var delegate:FilterViewDelegate?
    var rootSearch:SearchItemsModel? = nil
    var temporySearch:SearchItemsModel? =  SearchItemsModel()
    var minimumPrice = Constant.Min_Price {
        willSet{
            self.labelMinimumValue.text = "Rp. \(newValue)"
            self.temporySearch?.pmin = "\(newValue)"
        }
    }
    var maximumPrice = Constant.Max_Price {
        willSet{
            self.labelMaksimumValue.text = "Rp. \(newValue)"
            self.temporySearch?.pmax = "\(newValue)"
        }
    }
    var isWholeSale = false {
        willSet {
                        self.temporySearch?.isWholeSale = newValue
             self.switchWholeSale.isOn  = newValue
            
        }
    }
    
    var isGoldMerchant  = false{
        willSet {
            self.temporySearch?.isGoldMember = newValue
            if newValue {
                self.viewGoldMerchant.backgroundColor = UIColor.orange
            }else{
                self.viewGoldMerchant.backgroundColor = UIColor.white
            }
        }
    }
    
    var isOfficialStore = false{
        willSet {
            self.temporySearch?.isOfficial = newValue
            if newValue {
                self.viewOfficialStore.backgroundColor = UIColor.red
            }else{
                self.viewOfficialStore.backgroundColor = UIColor.white
            }
        }
    }
    @IBOutlet weak var viewOfficialStore: UIView!
    
    @IBOutlet weak var viewGoldMerchant: UIView!
    @IBOutlet weak var rangeSlider: TTRangeSlider!
    @IBOutlet weak var labelGoldMerchant: UILabel!
    @IBOutlet weak var labelOfficialStore: UILabel!
    @IBOutlet weak var labelMinimumValue: UILabel!
    @IBOutlet weak var labelMaksimumValue: UILabel!
    @IBOutlet weak var switchWholeSale: UISwitch!
    @IBOutlet weak var buttonGoldMerchant: UIButton!
    @IBOutlet weak var buttonOfficialStore: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.rangeSlider.delegate = self
        self.defaultValue()
        
    }
    func defaultValue(){
        self.viewOfficialStore.setCompoundShapes()
        self.buttonOfficialStore.setRounded()
        self.viewGoldMerchant.setCompoundShapes()
        self.buttonGoldMerchant.setRounded()
        self.minimumPrice =  Int((self.rootSearch?.pmin)!)!
        self.maximumPrice = Int((self.rootSearch?.pmax)!)!
        self.isWholeSale = (self.rootSearch?.isWholeSale)!
        self.isGoldMerchant  =  (self.rootSearch?.isGoldMember)!
        self.isOfficialStore =  (self.rootSearch?.isOfficial)!
        self.rangeSlider.step =  100
        self.rangeSlider.selectedMinimum =  Float(self.minimumPrice)
        self.rangeSlider.selectedMaximum =  Float(self.maximumPrice)
        
    }
    
    func reset(){
        self.minimumPrice =  Constant.Min_Price
        self.maximumPrice = Constant.Max_Price
        self.isWholeSale = false
        self.isGoldMerchant  =  false
        self.isOfficialStore =  false
        self.rangeSlider.selectedMinimum =  Float(self.minimumPrice)
        self.rangeSlider.selectedMaximum =  Float(self.maximumPrice)
    }
    
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        self.minimumPrice = Int(selectedMinimum)
        self.maximumPrice = Int(selectedMaximum)
    }
    
    
    @IBAction func actionWholesale(_ sender: Any) {
        self.isWholeSale = !self.isWholeSale
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionGoldMerchant(_ sender: Any) {
        
        self.isGoldMerchant = !self.isGoldMerchant
        
    }
    @IBAction func actionOfficialStore(_ sender: Any) {
        self.isOfficialStore = !self.isOfficialStore
    }
    
    
    @IBAction func actionReset(_ sender: Any) {
        reset()
    }
    
    
    @IBAction func actionDismis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionApply(_ sender: Any) {
        
        self.rootSearch?.pmin = (self.temporySearch?.pmin)!
        self.rootSearch?.pmax = (self.temporySearch?.pmax)!
        self.rootSearch?.isWholeSale = (self.temporySearch?.isWholeSale)!
        self.rootSearch?.isOfficial = (self.temporySearch?.isOfficial)!
        self.rootSearch?.isGoldMember = (self.temporySearch?.isGoldMember)!
        self.delegate?.refreshView()
        self.dismiss(animated: true, completion: nil)
    }
    func updateView(goldMerchant: Bool, officialStore: Bool) {
        self.isGoldMerchant = goldMerchant
        self.isOfficialStore = officialStore
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "ShopTypeSegue" ,
            let nextScene = segue.destination as? ShopTypeViewController {
            
            nextScene.delegate = self
            nextScene.isOfficialStore = self.isOfficialStore
            nextScene.isGoldMerchant = self.isGoldMerchant
            
        }
     }
 
    
}
