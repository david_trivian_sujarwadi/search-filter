//
//  ShopTypeViewController.swift
//  Search Filter
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import UIKit
protocol ShopTypeDelegate:class {
    func updateView(goldMerchant:Bool,officialStore:Bool)
}

class ShopTypeViewController: UIViewController {
    weak var delegate:ShopTypeDelegate?
    
    var isGoldMerchant =  false {
        willSet{
            if newValue {
                self.viewGoldMerchant?.backgroundColor = UIColor.gray
            }else{
                self.viewGoldMerchant?.backgroundColor = UIColor.white
            }
        }
    }
    
    var isOfficialStore = false{
        willSet {
            if newValue {
                self.viewOfficialStore?.backgroundColor = UIColor.gray
            }else{
                self.viewOfficialStore?.backgroundColor = UIColor.white
            }
        }
    }
    @IBOutlet weak var viewGoldMerchant: UIView!
    @IBOutlet weak var viewOfficialStore: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewOfficialStore.setBorder()
        self.viewGoldMerchant.setBorder()
        self.viewOfficialStore.backgroundColor = isOfficialStore ?UIColor.gray:UIColor.white
        self.viewGoldMerchant.backgroundColor = isGoldMerchant ? UIColor.gray:UIColor.white
    }
    
    
    @IBAction func actionOfficialStore(_ sender: Any) {
        self.isOfficialStore = !self.isOfficialStore
    }
   
    @IBAction func actionGoldMerchant(_ sender: Any) {
        self.isGoldMerchant = !self.isGoldMerchant
    }
    
    @IBAction func actionApply(_ sender: Any) {
        self.delegate?.updateView(goldMerchant: self.isGoldMerchant, officialStore: self.isOfficialStore)
           self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionReset(_ sender: Any) {
        self.isGoldMerchant = false
        self.isOfficialStore = false
    }
    @IBAction func actionDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
