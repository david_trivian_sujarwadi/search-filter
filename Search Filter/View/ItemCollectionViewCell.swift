//
//  ItemCollectionViewCell.swift
//  Search Filter
//
//  Created by David Trivian S on 11/28/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    @IBOutlet weak var imageviewThumbnail: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
