//
//  SearchViewController.swift
//  Search Filter
//
//  Created by David Trivian S on 11/29/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FilterViewDelegate  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var searchItemsModel:SearchItemsModel?
    var  fetching : Fetching?
    var downloading:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CellItem")
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        collectionView.refreshControl =  refreshControl
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        let attributedTitle = NSAttributedString(string: "Refreshing Search", attributes: attributes)
        refreshControl.attributedTitle =  attributedTitle
        refreshControl.beginRefreshing()
        
        self.fetching = Fetching()
        searchItemsModel = SearchItemsModel.init(fetching: fetching!)
        self.downloading =  true
        searchItemsModel?.listItem(refresh: true,refreshView:refreshCollectionView, beginUpdateView: beginUpdate, failed: failed, success: success)
    }
    
    //pull to refresh
    @objc func didPullToRefresh(){
        if downloading {
            if (self.collectionView.refreshControl?.isRefreshing)! {
                self.collectionView.refreshControl?.endRefreshing()
            }
        }else{
            downloading =  true
               searchItemsModel?.listItem(refresh: true,refreshView:refreshCollectionView, beginUpdateView: beginUpdate, failed: failed, success: success)
        }
        
    }
    
    //refresh collection view
    func refreshCollectionView(){
        DispatchQueue.main.async{
            if self.collectionView.numberOfItems(inSection: 0) != self.searchItemsModel?.numberOfRows(inSection: 0) {
                self.collectionView.reloadData()
            }else{
                self.collectionView.performBatchUpdates(nil, completion: nil)
            }
        }
    }
    //begin update
    func beginUpdate() {
        DispatchQueue.main.async{
            self.showToast(message: "Loading")
        }
        
    }
    
    //failed Handle
    func failed(_ error:Error?){
        downloading = false
        let message = self.searchItemsModel?.errorHandle(error)
        
        DispatchQueue.main.async{
            self.showToast(message: message!)
            if (self.collectionView.refreshControl?.isRefreshing)! {
                self.collectionView.refreshControl?.endRefreshing()
            }
        }
    }
    
    //succcess Handle
    func success(_ success:Bool){
        downloading = false
        if(success) {
            DispatchQueue.main.async{
                if (self.collectionView.refreshControl?.isRefreshing)! {
                    self.collectionView.refreshControl?.endRefreshing()
                }
                self.collectionView.reloadData()
                self.showToast(message: "Success")
            }
        }
    }
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.searchItemsModel?.numberOfRows(inSection: section))!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellItem", for: indexPath) as! ItemCollectionViewCell
        self.configureListCell(cell,indexPath: indexPath)
        return cell
        
    }
    
    func configureListCell(_ cell: ItemCollectionViewCell, indexPath: IndexPath) {
        let itemModel =  self.searchItemsModel?.itemForRow(at: indexPath)
        cell.labelTitle.text = itemModel?.name
        cell.labelPrice.text = itemModel?.price
        cell.imageviewThumbnail.image = UIImage(named:"toped")
        if itemModel?.imageThumbnailUrl != nil {
            self.searchItemsModel?.cacheImage(stringURl: itemModel?.imageThumbnailUrl!) { (success,image ) in
                if success {
                    DispatchQueue.main.async{
                        cell.imageviewThumbnail.image = image
                    }
                }
            }
        }
        
    }
    
    var updatedSize: CGSize!
    
    // Use the UICollectionViewDelegateFlowLayout to set the size of our
    // cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // We will set our updateSize value if it's nil.
        if updatedSize == nil {
            // At this point, the correct ViewController frame is set.
            self.updatedSize = self.view.frame.size
        }
        
        if self.updatedSize.width > self.updatedSize.height {
            let widthScreen = (self.updatedSize.width - 6)
            let widthNew = widthScreen  / 5
            
            return CGSize(width: widthNew  , height: widthNew * 7 / 5);
            
        } else {
            let widthScreen = (self.updatedSize.width - 2 )
            let widthNew = widthScreen / 2
            return CGSize(width: widthNew  , height: widthNew * 7 / 5);
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastElement = (self.searchItemsModel?.numberOfRows(inSection: 0))! - 1
        if !downloading && indexPath.row == lastElement {
            downloading = true
            self.searchItemsModel?.listItem(refresh: false, refreshView:refreshCollectionView,beginUpdateView: beginUpdate, failed: failed, success: success)
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.updatedSize = size
        self.collectionView!.performBatchUpdates(nil, completion: nil)
    }
    
    
    func refreshView(){
        self.downloading =  true
        self.collectionView.setContentOffset(.zero, animated: true)
        self.searchItemsModel?.listItem(refresh: true,refreshView:refreshCollectionView, beginUpdateView: beginUpdate, failed: failed, success: success)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FilterSegue" ,
            let nextScene = segue.destination as? FilterViewController {
            
            nextScene.delegate = self
            nextScene.rootSearch = self.searchItemsModel
            
        }
    }
    
    
}
