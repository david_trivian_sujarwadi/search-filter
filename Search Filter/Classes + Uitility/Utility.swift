//
//  Utility.swift
//  Contacts
//
//  Created by David Trivian S on 9/11/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import Foundation
import UIKit
//handle invalid response status code
extension URLResponse {
    func isHTTPResponseValid() -> Bool {
        let httpResponse = self as? HTTPURLResponse
        return ((httpResponse?.statusCode)! >= 200 && (httpResponse?.statusCode)! <= 299)
        
    }
    
}
//set Round layer
extension UIButton {
    func setRounded() {
        
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
        
    }
    
   
}
extension UIImageView {
    //set Round layer
    func setRounded() {
        
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
        
    }
    //add Gradient Layer
    func addGradientLayer(frame: CGRect){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [UIColor.white.cgColor, UIColor.white.cgColor, UIColor(red: 102.0/255.0, green: 255.0/255.0, blue: 204.0/255.0, alpha: 1).cgColor]
        gradient.locations = [0.0, 0.5]
        self.layer.addSublayer(gradient)
    }
}

extension UIView {
    func setCompoundShapes() {
        
        self.layer.masksToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 25
        self.clipsToBounds = true
        
    }
    func setBorder(){
        self.layer.masksToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.black.cgColor
    }
}
extension UIViewController {
    //toast meessage
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont .systemFont(ofSize: 12)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        self.view.bringSubview(toFront: toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
extension UIScrollView {
    
    
    var isAtBottom: Bool {
        return contentOffset.y >= verticalOffsetForBottom
    }
    
    
    var verticalOffsetForBottom: CGFloat {
        let scrollViewHeight = bounds.height
        let scrollContentSizeHeight = contentSize.height
        let bottomInset = contentInset.bottom
        let scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight
        return scrollViewBottomOffset
    }
}



