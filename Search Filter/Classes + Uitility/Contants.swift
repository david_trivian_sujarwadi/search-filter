//
//  Contants.swift
//  Contacts
//
//  Created by David Trivian S on 9/11/17.
//  Copyright © 2017 David Trivian S. All rights reserved.
//

import Foundation
//all constant must here
struct Constant {
    static let RootServer = "https://ace.tokopedia.com/search/v2.5"
    static let Path_Product = "product"
    static let Min_Price = 10000
    static let Max_Price =  100000
    static let Search_Keyword = "samsung"
}
